# Dependency compiler

Simple dependency compiler for Python using Gitlab CI.

## Usage

Edit the files in the `input/` directory for generic dependencies.

The files in the overrides/_python\_version_/ subdirectories will override
the respective files in the aforementioned `input/` directory.

The pipeline uses pip-compile to determine pinned dependencies per configured
Python version (currently, `3.6` and `3.9` are configured).


